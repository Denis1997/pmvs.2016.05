#!/bin/bash
sudo echo "15" > /proc/first
sudo echo "4" > /proc/second
sudo echo "/" > /proc/operator
value="$(sudo cat /dev/result | colrm 3)"
[ "3" == $value ] && echo "15 / 4 = 3  --OK"
[ "3" != $value ] && echo "15 / 4 = 3  --FAILED"
sudo echo "10" > /proc/first
sudo echo "2" > /proc/second
sudo echo "*" > /proc/operator
value="$(sudo cat /dev/result | colrm 3)"
[ "20" == $value ] && echo "10 * 2 = 20  --OK"
[ "$value" != "20" ] && echo "10 * 2 = 20  --FAILED"
sudo echo "14" > /proc/first
sudo echo "8" > /proc/second
sudo echo "-" > /proc/operator
value="$(sudo cat /dev/result | colrm 2)"
[ "$value" == "6" ] && echo "14 - 8 = 6  --OK"
[ "$value" != "6" ] && echo "14 - 8 = 6  --FAILED"
sudo echo "5" > /proc/first
sudo echo "6" > /proc/second
sudo echo "+" > /proc/operator
value="$(sudo cat /dev/result | colrm 3)"
[ "$value" == "11" ] && echo "5 + 6 = 11  --OK"
[ "$value" != "11" ] && echo "5 + 6 = 11  --FAILED"
