#include <linux/fs.h>
#include <linux/init.h>
#include <linux/miscdevice.h>
#include <linux/module.h>
#include <linux/proc_fs.h>
#include <asm/uaccess.h>

#define PROCFS_MAX_SIZE		100

static char procfirst_buffer[PROCFS_MAX_SIZE];
static char procsecond_buffer[PROCFS_MAX_SIZE];
static char procoperator_buffer[PROCFS_MAX_SIZE];
static int first = 0;
static int second = 0;
static char operator = ' ';
static int ans = 0;

int atoi(const char *str)
{
	int number = 0;
	int ret = 0;
	while (*str) {
		if (*str < '0' || *str > '9')
			return ret;
		ret = ret * 10 + ((int)(*str - '0'));
		str++;
		number++;
		if (number > 40)
			return 0;
	}
	return ret;
}

void recalc_ans()
{
	switch(operator)
	{
		case '+':
			ans = first + second;
			break;
		case '-':
			ans = first - second;
			break;
		case '*':
			ans = first * second;
			break;
		case '/':
			if (second == 0)
			{
				ans = 0;
				break;
			}
			ans = first / second;
			break;
		default:
			ans = 0;
	}
}

int procfirst_write(struct file *file, const char *buffer, unsigned long count,
		   void *data)
{
	/* get buffer size */
	unsigned long procfs_buffer_size = count;
	if (procfs_buffer_size > PROCFS_MAX_SIZE ) {
		procfs_buffer_size = PROCFS_MAX_SIZE;
	}

	/* write data to the buffer */
	if ( copy_from_user(procfirst_buffer, buffer, procfs_buffer_size) ) {
		return -EFAULT;
	}

	first = atoi(procfirst_buffer);
	recalc_ans();
	return procfs_buffer_size;
}

int procfirst_read(char *buf,char **start,off_t offset,int count,int *eof,void *data )
{
	int len=0;
	len = sprintf(buf,"\n %s\n ", procfirst_buffer);

	return len;
}

int procsecond_write(struct file *file, const char *buffer, unsigned long count,
		   void *data)
{
	/* get buffer size */
	unsigned long procfs_buffer_size = count;
	if (procfs_buffer_size > PROCFS_MAX_SIZE ) {
		procfs_buffer_size = PROCFS_MAX_SIZE;
	}

	/* write data to the buffer */
	if ( copy_from_user(procsecond_buffer, buffer, procfs_buffer_size) ) {
		return -EFAULT;
	}
	second = atoi(procsecond_buffer);
	recalc_ans();
	return procfs_buffer_size;
}

int procsecond_read(char *buf,char **start,off_t offset,int count,int *eof,void *data )
{
	int len=0;
	len = sprintf(buf,"\n %s\n ", procsecond_buffer);

	return len;
}

int procoperator_write(struct file *file, const char *buffer, unsigned long count,
		   void *data)
{
	unsigned long procfs_buffer_size = count;
	/* get buffer size */

	if (procfs_buffer_size > PROCFS_MAX_SIZE ) {
		procfs_buffer_size = PROCFS_MAX_SIZE;
	}

	/* write data to the buffer */
	if ( copy_from_user(procoperator_buffer, buffer, procfs_buffer_size) ) {
		return -EFAULT;
	}
	operator = procoperator_buffer[0];
	recalc_ans();
	return procfs_buffer_size;
}

int procoperator_read(char *buf,char **start,off_t offset,int count,int *eof,void *data )
{
	int len=0;
	len = sprintf(buf,"\n %s\n ", procoperator_buffer);

	return len;
}
/*
 * hello_read is the function called when a process calls read() on
 * /dev/hello.  It writes "Hello, world!" to the buffer passed in the
 * read() call.
 */

static ssize_t result_read(struct file * file, char * buf, 
			  size_t count, loff_t *ppos)
{
	char result_str[100];
	int len;
	sprintf(result_str, "%d", ans);
	len = strlen(result_str);
	/*
	 * We only support reading the whole string at once.
	 */
	if (count < len)
		return -EINVAL;
	/*
	 * If file position is non-zero, then assume the string has
	 * been read and indicate there is no more data to be read.
	 */
	if (*ppos != 0)
		return 0;
	/*
	 * Besides copying the string to the user provided buffer,
	 * this function also checks that the user has permission to
	 * write to the buffer, that it is mapped, etc.
	 */
	if (copy_to_user(buf, result_str, len))
		return -EINVAL;
	/*
	 * Tell the user how much data we wrote.
	 */
	*ppos = len;

	return len;
}

/*
 * The only file operation we care about is read.
 */

static struct proc_dir_entry* first_fops;

static struct proc_dir_entry* second_fops;

static struct proc_dir_entry* operator_fops;

static struct file_operations result_fops = {
	.owner		= THIS_MODULE,
	.read		= result_read,
};

static struct miscdevice result_dev = {
	/*
	 * We don't care what minor number we end up with, so tell the
	 * kernel to just pick one.
	 */
	MISC_DYNAMIC_MINOR,
	/*
	 * Name ourselves /dev/hello.
	 */
	"result",
	/*
	 * What functions to call when a program performs file
	 * operations on the device.
	 */
	&result_fops
};
static int __init
calculator_init(void)
{
	int ret;

	/*
	 * Create the "hello" device in the /sys/class/misc directory.
	 * Udev will automatically create the /dev/hello device using
	 * the default rules.
	 */
	ret = misc_register(&result_dev);
	if (ret)
		printk(KERN_ERR
		       "Unable to register \"Hello, world!\" misc device\n");
	/*if (create_proc_read_entry("first", 0, NULL, hello_read_proc,
                                    NULL) == 0) {
                printk(KERN_ERR
                       "Unable to register \"Hello, world!\" proc file\n");
        }*/
	first_fops = create_proc_entry("first", 0666, NULL);
	first_fops->write_proc = procfirst_write;
	first_fops->read_proc = procfirst_read;
	//first_fops.owner = THIS_MODULE;

	second_fops = create_proc_entry("second", 0666, NULL);
	second_fops->write_proc = procsecond_write;
	second_fops->read_proc = procsecond_read;
	//second_fops.owner = THIS_MODULE;

	operator_fops = create_proc_entry("operator", 0666, NULL);
	operator_fops->write_proc = procoperator_write;
	operator_fops->read_proc = procoperator_read;
	//operator_fops.owner = THIS_MODULE;
	return ret;
}

module_init(calculator_init);

static void __exit
calculator_exit(void)
{
	misc_deregister(&result_dev);
	remove_proc_entry("first", NULL);
	remove_proc_entry("second", NULL);
	remove_proc_entry("operator", NULL);
}

module_exit(calculator_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Shilo Denis");
MODULE_DESCRIPTION("Calculator.");
MODULE_VERSION("1.1.123.0");
